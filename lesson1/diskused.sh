#¡/bin/bash 

echo "**************************************"
echo "Verifying the used space on the server"
echo "**************************************"
#echo "%s %s\n"
printf "%s %s\n" "$(date)" "$line"
df -a | tee reports-$(date '+%Y|%m|%d-%H:%M').log
echo "**************************************"
echo "Verification completed successfully"
echo "**************************************"
