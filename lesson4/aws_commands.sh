#!/bin/bash

response=$1

function runAllAwsCommands() {
    echo $1
    echo ''
    echo '**************************'
    echo 'Executing all aws commands:'
    echo '***************************'
    echo ''
    echo 'Listing accounts within organisation:'
    /usr/local/bin/aws organizations list-accounts --output table

    echo ''
    echo 'Listing subnets within VPC:'
    /usr/local/bin/aws ec2 describe-subnets --query "Subnets[].SubnetId" --output table
    echo ''
    echo 'Listing all IamUsers:'
    /usr/local/bin/aws iam list-users --output table
    echo ''
    echo 'Listing all S3 buckets'
    /usr/local/bin/aws s3api list-buckets --output table
    echo ''
    echo 'Listing Route53 Hosted Zones:'
    /usr/local/bin/aws route53 list-hosted-zones --output table
    echo ''
    echo 'Listing all EC2 instances:'
    /usr/local/bin/aws ec2 describe-instances --output table
    echo 'Listing all VPCs: '
    /usr/local/bin/aws ec2 describe-vpcs --output table
}
runAllAwsCommands
