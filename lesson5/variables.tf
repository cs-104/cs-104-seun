variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {}
variable "region" {}
variable "env_tag" {}
variable "dns_hostname" {}
variable "dns_support" {}
variable "inst_tanancy" {}
variable "ip_range" {}
variable "associate_pub_ip" {}
variable "private_key_path" {
  type    = string
  default = "/Users/oluseunorebajo/felixDevOps/lesson2/cs-104-seun/lesson5/olu-dev-mindz-key.pem"
}

variable "pub_ip_range" {
  description = "Specify public subnet ranges "
  type        = list(any)
  default     = []
}

variable "priv_ip_range" {
  description = "Specify private ip ranges for private subnet"
  type        = list(any)
  default     = []
}
