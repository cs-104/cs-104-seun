provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.region
}

resource "aws_instance" "seun-centos7-instance" {
  ami                    = "ami-04f5641b0d178a27a"
  instance_type          = "t2.micro"
  key_name               = var.aws_key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]

  tags = {
    Name = "seun-instance-server"
  }

}

resource "aws_instance" "ngnix-centos7" {
  ami                         = "ami-04f5641b0d178a27a"
  instance_type               = "t2.micro"
  key_name                    = var.aws_key_name
  subnet_id                   = aws_subnet.pub_subnet[0].id
  associate_public_ip_address = var.associate_pub_ip
  vpc_security_group_ids      = [aws_security_group.ngnx-sgrp.id]

  tags = {
    Name = "ngnix"
  }
}

resource "aws_vpc" "my_vpc" {
  cidr_block           = var.ip_range
  enable_dns_support   = var.dns_support
  enable_dns_hostnames = var.dns_hostname
  instance_tenancy     = var.inst_tanancy
  tags = merge({
    "Name" = format("%s-vpc", var.env_tag)
  })

}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.my_vpc.id

}
resource "aws_subnet" "pub_subnet" {
  count = length(var.pub_ip_range)
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = var.pub_ip_range[count.index]
  map_public_ip_on_launch = "true"
  availability_zone       = "eu-west-1a"

  tags = {
    Name = "cs-104-lesson6-seun-pub"
  }
}


resource "aws_route_table" "rtb" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "rta-pubsub" {
  subnet_id      = aws_subnet.pub_subnet[0].id
  route_table_id = aws_route_table.rtb.id
}

resource "aws_subnet" "priv_subnet" {
  count  = length(var.priv_ip_range)
  vpc_id = aws_vpc.my_vpc.id
  cidr_block        = var.priv_ip_range[count.index]
  availability_zone = "eu-west-1b"

  tags = {
    Name = "cs-104-lesson6-seun-priv"
  }
}

resource "aws_default_vpc" "default" {

}

resource "aws_security_group" "allow_ssh" {
  name        = "centos-instance"
  description = "Allow ports for centos instance"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ngnx-sgrp" {
  name        = "ngnix-security-group"
  description = "Allow connection to the ngnix server"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

