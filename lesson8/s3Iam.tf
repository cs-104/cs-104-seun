provider "aws" {
    region = "eu-west-1"
    assume_role {
    role_arn = "arn:aws:iam::023451010066:role/OrganizationAccountAccessRole"
    external_id  = "EXTERNAL_ID"
  }
}

module "user" {
    source = "./iam/tf-iam-module"
    username = "olutestUser"
}

module "s3bucket" {
    source = "./s3/tf-s3-module"
    bucket = "s3bucket-${terraform.workspace}"
    acl = "private"
}

