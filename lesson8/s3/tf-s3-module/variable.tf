variable "bucket" {
    description = "Provide a bucket name to create"
    default = ""
}

variable "acl" {
    description = "Provide bucket pernission"
    default = ""
}

# variable "bucket_name_prefix" {

# }
